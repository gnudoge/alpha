#!/bin/bash
ftp_username="init_value"
ftp_password="init_value"
ftp_server="init_value"
mis_usage= '245'

if [ $# -eq 0 ] ; then
        echo "Usage: $0 server username password"
        exit ${mis_usage}
fi

ftp_server="$(echo $1)"
ftp_username="$(echo $2)"
ftp_password="$(echo $3)"

eval lftp -u '${ftp_username}','${ftp_password}' 'ftp://${ftp_server}/' -e 'set cmd:move-background-detach false ; set xfer:log true ; set ftp:list-options -a ; set ftp:ssl-allow false ; set net:connection-limit 1 ; set net:limit-total-rate 3200000 ; set cache:enable false'

unset ftp_username
unset ftp_password
unset ftp_server

exit 0